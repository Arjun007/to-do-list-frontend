# TodoList

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

**Run**

To run the project, first write `npm install` and then `npm start`. Frontend app will start running on port **4200**.  Start Node server before Angular.

**Functionalities**

 - Use of reactive forms for form validation and other input related tasks
 - Proper MVC structure
 - Authgaurd Setup 
 - Http-intercepter setup for middleware during API calls
 - Socket connection and event handle management
 - Setup routes and modules
