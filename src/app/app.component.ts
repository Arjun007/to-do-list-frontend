import { Component, OnInit } from '@angular/core';
import { Spinkit } from 'ng-http-loader';
import { ToasterConfig, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'todo-list';
  public spinkit = Spinkit;
  public config: ToasterConfig = new ToasterConfig({
    limit: 1,
    preventDuplicates: true,
    bodyOutputType: BodyOutputType.TrustedHtml
  });


  ngOnInit() {

  }
}
