import { UtilService } from './../../core/services/util.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { passwordValidator } from 'src/app/core/services/app-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.utilService.clearLocalStorage();
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, passwordValidator])],
    });
  }

  /**
   * Call login API
   */
  login() {
    if (this.loginForm.valid) {
      console.log(this.loginForm.value);
      const postObj = {
        email: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value,
      }

      this.authService.loginUser(postObj).then((res: any) => {
        console.log('res', res);
        if (res.success) {
          this.utilService.showSuccess('Success', res.message);
          this.utilService.setLocalStorageData('userdata', res.data);
          this.utilService.setToken(res.data.token);
          this.router.navigate(['/list']);
        } else {
          this.utilService.showError('Error', res.message);
        }
      }).catch(err => {
        console.log('error',err);
        if (err.hasOwnProperty('error')) {
          this.utilService.errorHandler(err.error);
        } else {
          this.utilService.showError('Error', this.utilService.commonErrorMsg);
        }
      });


    }

  }

}
