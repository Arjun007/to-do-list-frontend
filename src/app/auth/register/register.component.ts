import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/services/util.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { emailValidator, passwordValidator } from 'src/app/core/services/app-validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    private utilService: UtilService,
    private authSerivce: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.utilService.clearLocalStorage();
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z \-\']+')])],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      password: ['', Validators.compose([Validators.required, passwordValidator, Validators.minLength(5)])],
    });
  }

  submitFrom() {
    if (this.registerForm.valid) {
      const postObj = {
        name: this.registerForm.controls.username.value,
        email: this.registerForm.controls.email.value,
        password: this.registerForm.controls.password.value
      }

      this.authSerivce.registerUser(postObj).then((res: any) => {
        console.log('res', res);
        if (res.success) {
          this.utilService.showSuccess('Success', res.message);
          this.router.navigate(['/auth']);

        } else {
          this.utilService.showError('Error', res.message);
        }
      }) .catch(err => {
        console.log(err);
        if (err.hasOwnProperty('error')) {
          this.utilService.errorHandler(err.error);
        } else {
          this.utilService.showError('Error', this.utilService.commonErrorMsg);
        }
      });
    }
  }

}
