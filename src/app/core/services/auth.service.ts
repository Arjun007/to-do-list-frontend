import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { apiInfo } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private requestService: RequestService
  ) { }

  /**
   * Login Api
   */
  loginUser(postObj: any) {
    return new Promise((resolve, reject) => {
      const headers = [{
          key: 'Accept',
          name: 'application/json'
      }];
      this.requestService.post(apiInfo.info.loginApi, postObj, headers)
      .then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
  }

  /**
   * Register User
   */
  registerUser(postObj: any) {
    return new Promise((resolve, reject) => {
      const headers = [{
          key: 'Accept',
          name: 'application/json'
      }];
      this.requestService.post(apiInfo.info.registerApi, postObj, headers)
      .then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
  }

}
