import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';
import { ConnectionService } from 'ng-connection-service';
import { environment } from 'src/environments/environment';
// BaseApi variable
const baseApi = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  options: any;
  isConnected: boolean;
  constructor(
    private http: HttpClient,
    private connectionService: ConnectionService,
    private utilService: UtilService
  ) {
    const headers = new HttpHeaders();
    this.options = { headers };
  }

  checkConnection() {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
    });
  }

   /**
    * Used for calling GET method on server and pass API url in calling function
    * @param endpoint: any
    */

    get(endpoint) {
    return new Promise((resolve, reject) => {
      this.http.get(baseApi + endpoint, this.options)
      .subscribe(res => {
          resolve(res);
      }, (err) => {
          reject(err);
      });
    });
  }

  /**
   * Used for calling POST method on server and pass API url/ POST Data / Header Options in calling function
   * @param endpoint: any
   */

  post(endpoint, body, headers = []) {
    this.isConnected = navigator.onLine;
    if (headers.length) {
        const that = this;
  // tslint:disable-next-line: only-arrow-functions
        headers.forEach(function(key, val) {
            that.options.headers.set(key.key, key.name);
        });
    }
    return new Promise((resolve, reject) => {
      if (this.isConnected) {
        this.http.post(baseApi + endpoint, body, this.options)
        .subscribe(res => {
            resolve(res);
        }, (err) => {
            reject(err);
        });
      } else {
        this.utilService.showError('Error', 'Please check your internet connection');
      }
    });
  }

   /**
    * Used for calling PUT method on server and pass API url/ POST Data / Header Options in calling function
    * @param endpoint: any
    */
   put(endpoint, body, headers = []) {
    this.isConnected = navigator.onLine;
    if (headers.length) {
        const that = this;
  // tslint:disable-next-line: only-arrow-functions
        headers.forEach(function(key, val) {
            that.options.headers.set(key.key, key.name);
        });
    }
    return new Promise((resolve, reject) => {
      if (this.isConnected) {
        this.http.put(baseApi + endpoint, body, this.options)
        .subscribe(res => {
            resolve(res);
        }, (err) => {
            reject(err);
        });
      } else {
        console.log('Please check your internet connection');
      }
    });
  }

  /**
   * Used for calling DELETE method on server and pass API url/ POST Data / Header Options in calling function
   * @param endpoint:any
   */
  delete(endpoint, body, headers = []) {
    this.isConnected = navigator.onLine;
    if (headers.length) {
        const that = this;
  // tslint:disable-next-line: only-arrow-functions
        headers.forEach(function(key, val) {
            that.options.headers.set(key.key, key.name);
        });
    }
    this.options.body = body;
    return new Promise((resolve, reject) => {
      if (this.isConnected) {
        this.http.delete(baseApi + endpoint, this.options)
        .subscribe(res => {
            resolve(res);
        }, (err) => {
            reject(err);
        });
      } else {
        console.log('Please check your internet connection');
      }
    });
  }

}
