import { Injectable } from '@angular/core';
import { ToasterService, BodyOutputType, Toast } from 'angular2-toaster';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  private authToken: string;
  private  isShowMsg: any;
  public commonErrorMsg: any = 'Something went wrong. Please try again later';

  constructor(
    private toasterService: ToasterService
  ) { }

  /**
   * Set localstorage data
   */
  setLocalStorageData(key, value) {
    const enc = JSON.stringify(value);
    localStorage.setItem(key, window.btoa(unescape(encodeURIComponent(enc))));
  }

  /**
   * Get the localstorage data
   */
  getLocalStorageData(key) {
    if (localStorage.getItem(key)) {
      // const dec = window.atob(localStorage.getItem(key));
      const dec = decodeURIComponent(
        escape(window.atob(localStorage.getItem(key)))
      );
      if (dec !== undefined && dec !== null && dec !== 'undefined') {
        return JSON.parse(dec);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  /**
   * Clear localstorage data
   */
  clearLocalStorage() {
    localStorage.clear();
  }

  /**
   * Remove particular key from storage
   */
  removeLocalStorageData(key) {
    localStorage.removeItem(key);
  }

  /**
   * get local storage token
   */
  getToken(): string | null {
    if (this.authToken) {
      return this.authToken;
    } else {
      return localStorage.getItem('token')
        ? localStorage.getItem('token')
        : null;
    }
  }

  /**
   * set token in localstorage
   * @param token: string
   */
  setToken(token: string): void {
    this.authToken = token;
    localStorage.setItem('token', token);
  }

  /**
   * Check user is login or not
   */
  isLoggedIn(): boolean {
    return localStorage.getItem('token') && localStorage.getItem('token')
      ? true
      : false;
  }

  /**
   * Show error message
   */
  showError(title: string, message: string) {
    if (this.isShowMsg !== undefined) {
      this.toasterService.clear();
    }
    const toast: Toast = {
      type: 'error',
      title,
      body: `<p>${message}</p>`,
      bodyOutputType: BodyOutputType.TrustedHtml
  };
    this.isShowMsg = this.toasterService.popAsync(toast);
  }

  /**
   * Show success message
   */
  showSuccess(title: string, message: string) {
    if (this.isShowMsg !== undefined) {
      this.toasterService.clear();
    }
    this.isShowMsg = this.toasterService.popAsync('success', title, message);
  }

  /**
   * Common error handler
   */
  errorHandler(error) {
    const title = '';
    if (error && error.hasOwnProperty('message')) {
      if (typeof error.message === 'object') {
        for (const x of Object.keys(error.message)) {
          this.showError(title, error.message[x]);
        }
      } else if (typeof error.message === 'string') {
        this.showError(title, error.message);
      }
    } else {
      // do not display something went wrong error for unauthorized request
      if (error.error && error.error !== 'invalid_token') {

        this.commonErrorMsg = 'Something went wrong. Please try again later';
        this.showError(title, this.commonErrorMsg);
      }
    }
  }
}
