import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService implements CanActivate{

  constructor(
    private router: Router,
    private utilService: UtilService,
    private route: ActivatedRoute
  ) { }

  /**
   * User is login or not for access route
   */
  canActivate(): boolean {
    if (this.utilService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/'], { relativeTo: this.route });
      return false;
    }
  }
}
