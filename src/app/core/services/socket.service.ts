import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  public socket: any;
  constructor() { }

  socketConnection(userId: any) {
    this.socket = io(environment.baseUrl, {
      query: `userId=${userId}`,
      transports: ["websocket"]
    });
  }

  onConnect() {
    this.socket.on('connect', () => {
      console.log('socket is connected', this.socket);
    })
  }

  public onImidiateExit = () =>  {
    return Observable.create(observer => {
      this.socket.on('imidiateexit', res => {
        console.log('getting res');
        observer.next(res);
      });
    });
  }
}
