import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { apiInfo } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private requestService: RequestService
  ) { }

  /**
   * Create new task
   */

  createNewTask(postObj, userId) {
    return new Promise((resolve, reject) => {
      const headers = [{
          key: 'Accept',
          name: 'application/json'
      }];
      this.requestService.post(apiInfo.info.addTaskApi +'/'+userId, postObj, headers)
      .then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
  }

  /**
   * Edit task
   */
  editTask (postObj, userId) {
    return new Promise((resolve, reject) => {
      const headers = [{
          key: 'Accept',
          name: 'application/json'
      }];
      this.requestService.put(apiInfo.info.updateTaskApi + '/' +userId, postObj, headers)
      .then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
  }

  /**
   * Delete task
   */
  deleteTask(postObj, userId) {
    return new Promise((resolve, reject) => {
      const headers = [{
          key: 'Accept',
          name: 'application/json'
      }];
      this.requestService.delete(apiInfo.info.deleteTaskApi+ '/' +userId, postObj, headers)
      .then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
  }

}
