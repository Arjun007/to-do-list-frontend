import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { UtilService } from './util.service';
import { SpinnerVisibilityService } from 'ng-http-loader';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';

@Injectable()
export class TodoHttpInterceptor implements HttpInterceptor {

    constructor(
        private utilService: UtilService,
        private spinner: SpinnerVisibilityService,
        private router: Router

    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Clone the request to add the new header.


        const authReq = req.clone({
               setHeaders: {
                Authorization: 'Bearer ' + `${this.utilService.getToken()}`,
            }
        });

        return next.handle(authReq).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // do stuff with response if you want

                }

                return event;
            }),
             catchError((error: HttpErrorResponse) => {
                 if ( (error.status === 401 || error.status === 403) && this.utilService.isLoggedIn() ) {
                   console.log('error status', error.status);
                   this.utilService.clearLocalStorage();
                   this.router.navigate(['/']);
                 }
                 this.spinner.hide();
                 return throwError(error);

          })
        );
    }
}
