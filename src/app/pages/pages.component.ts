import { SocketService } from './../core/services/socket.service';
import { UtilService } from './../core/services/util.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, OnDestroy{

  public sub: any;
  constructor(
    private utilService: UtilService,
    private socketService: SocketService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.utilService.isLoggedIn()) {
      const userData =  this.utilService.getLocalStorageData('userdata');
      const id = userData.id;
      this.socketService.socketConnection(id);
      this.socketService.onConnect();
    }

    // On imidiate exit message come
    this.sub = this.socketService.onImidiateExit().subscribe((res: any) => {
      console.log('res', res);
      this.utilService.showError('Error', 'You are logged in from other device');
      this.router.navigate(['/']);
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
