import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { TaskListComponent } from './task-list/task-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';



@NgModule({
  declarations: [PagesComponent, TaskListComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    Ng2SmartTableModule
  ]
})
export class PagesModule { }
