import { UserService } from './../../core/services/user.service';
import { HttpClient } from '@angular/common/http';
import { environment, apiInfo } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/core/services/util.service';
import { ServerDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  constructor(
    private utilService: UtilService,
    private http: HttpClient,
    private userService: UserService
  ) { }

  public userId: any;
  public source: ServerDataSource;

  settings = {
    actions: {
      add: true,
      delete: true,
      edit: true
    },
    add: {
      confirmCreate: true
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="fa fa-edit"></i>',
      saveButtonContent: '<i class="fa fa-check"></i>',
      cancelButtonContent: '<i class="fa fa-times"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="fa fa-trash"></i>'
    },
    columns: {

        task_name: {
          title: 'Task Name',
          type: 'string'
        },
        task_description: {
          title: 'Task Description',
          type: 'string'
        },
         updated_at: {
          title: 'Date',
          editable: false,
          addable: false
        }

    }
  };

  ngOnInit(): void {
    const data = this.utilService.getLocalStorageData('userdata');
    this.userId = data.id;

    const config = {
      endPoint: environment.baseUrl + apiInfo.info.getTasksApi +'/'+this.userId,
      dataKey: 'data'
    }
    this.source = new ServerDataSource(this.http, config);
    }

    // On create new task
    createNewTask(event): any {
      console.log('event', event);
      const postObj = {
        taskname: event.newData.task_name,
        description: event.newData.task_description
      }

      // Call create task api
      this.userService.createNewTask(postObj, this.userId).then((res: any) => {
        if (res.success) {
          this.utilService.showSuccess('Success', res.message);
          event.confirm.resolve()
        } else {
          this.utilService.showError('Error', res.message);
        }

      }).catch(err => {
        console.log(err);
        if (err.hasOwnProperty('error')) {
          this.utilService.errorHandler(err.error);
        } else {
          this.utilService.showError('Error', this.utilService.commonErrorMsg);
        }
      });
    }

    // On delete new task
    deleteTask(event) {
      if (window.confirm('Are you sure you want to delete this task?')) {
        const postObj = {
          taskid: event.data.id,
        }

        // Call create task api
        this.userService.deleteTask(postObj, this.userId).then((res: any) => {
          if (res.success) {
            this.utilService.showSuccess('Success', res.message);
            event.confirm.resolve()
          } else {
            this.utilService.showError('Error', res.message);
          }

        }).catch(err => {
          console.log(err);
          if (err.hasOwnProperty('error')) {
            this.utilService.errorHandler(err.error);
          } else {
            this.utilService.showError('Error', this.utilService.commonErrorMsg);
          }
        });
      } else {
        event.confirm.reject();
      }

    }

    // On edit new task
    editTask(event) {
      const postObj = {
        taskid: event.newData.id,
        taskname: event.newData.task_name,
        description: event.newData.task_description
      }

      // Call create task api
      this.userService.editTask(postObj, this.userId).then((res: any) => {
        if (res.success) {
          this.utilService.showSuccess('Success', res.message);
          event.confirm.resolve()
        } else {
          this.utilService.showError('Error', res.message);
        }

      }).catch(err => {
        console.log(err);
        if (err.hasOwnProperty('error')) {
          this.utilService.errorHandler(err.error);
        } else {
          this.utilService.showError('Error', this.utilService.commonErrorMsg);
        }
      });
    }

}
