export const environment = {
  production: true,
  baseUrl: 'http://localhost:3000/',
};


export const apiInfo = {
  endpoint: '',
  version: 'v1/',
  perpage: 10,
  info: {
    loginApi: 'login',
    registerApi: 'register',
    getTasksApi: 'gettasks',
    addTaskApi: 'createtask',
    updateTaskApi: 'updatetask',
    deleteTaskApi: 'deletetask'
  }
}
